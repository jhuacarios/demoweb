<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contacto</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/pricing_table.css">
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <script src="app.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
          <button class="close-button" aria-label="Close menu" type="button" data-close>
          <span aria-hidden="true">&times;</span>
          </button>
          <!-- Menu Lateral -->
          <?php require 'codigos/lateral-menu.php';?>
        </div>
        <div class="off-canvas-content" data-off-canvas-content>
          <?php require 'codigos/top-menu.php';?>
          <h2 style="font-size: 2em">Contacto</h2>
          <div class="claro">
            <div class="row align-middle">
              <img src="imagenes/contacto.jpg">
              <div class="small-12 medium-12 large-12 columns" style="padding-left: 0px; padding-right: 0px">
                <div class="recuadro-detalles" style="padding-left: 1em; padding-right: 1em">
                  <div class="row contacto">
                    <div style="padding-left:1em; padding-right:1em" class="small-12 medium-6">
                      <h5>Para cotizar</h5>
                      <p>Se le solicita adjuntar los siguientes datos en un mensaje:</p>
                      <ul>
                        <li>-Nombre completo</li>
                        <li>-Empresa</li>
                        <li>-Rubro del negocio</li>
                        <li>-Plan requerido</li>
                        <li>-Direccion del sitio a registrar</li>
                        <li>-Si tiene alguna idea en mente de lo que desea (colores, formas, fuentes, etc)</li>
                        <li>-Nombre de las secciones y su contenido en forma general</li>
                        <li>-Otros sitios web de referencia (opcional)</li>
                      </ul>
                      <br>
                      <p style="text-align: justify">En un plazo de 24 horas recibirá una maqueta simple del diseño solo para organizar la informacion a mostrar, si está de acuerdo, deberá enviar todos los datos restantes de forma detallada e incluyendo imágenes, junto un depósito prévio por $10.000 para iniciar el registro del dominio y correos electrónicos, o puede pagar la totalidad desde el principio.</p>
                    </div>
                    <div style="padding-left:1em; padding-right:1em" class="small-12 medium-6">
                    
                      <form id="myform" action="mailer.php" method="post">
                        <h5 style="text-align:center">Formulario de contacto</h5>
                        <h6 style="text-align:center"><strong>contacto@difusionweb.cl</strong> | (+56)9-62342699</h6>
                        <div id="form-messages" style="color:#000000; padding-bottom:10px; font-weight:bold; text-align: center"></div>
                        <label>Ingresa tu nombre
                        <input id="name" type="text" name="name" placeholder="Nombre completo" required pattern="[a-zA-Z\s]+"/>
                        </label>
                        <label>Ingresa tu correo electrónico
                        <input id="email" type="email" name="email" placeholder="Correo válido" required />
                        </label>
                        <label>¿Cuánto es 2+2?
                        <input id="verificar" type="text" name="verificar" placeholder="" required pattern="[4-4]" />
                        </label>
                        <label>Mensaje
                        <textarea style="min-height: 10em" id="message" name="message" placeholder="Tu mensaje" required></textarea>
                        </label>
                        <input auto" id="submit" type="submit" name="submit" class="success button" value="Enviar"> 
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer>
            <div class="row">
              <div class="small-12 medium-6">
                <img class="imagen-footer" src="imagenes/logo-footer.jpg">
              </div>
              <div class="small-12 medium-6 links">
                <div class="row"  style="max-width: 600px">
                  <div style="font-size:1.3em; color: #004d00; font-family: 'Patua One'" class="small-12">
                    PLANES
                    <hr>
                  </div>
                </div>
                <br>
                <div style="color: #336600; max-width: 570px" class="row links">
                  <div class="small-12 medium-3">
                    <strong><a href="plan-basico.php">BÁSICO</a></strong>
                  </div>
                  <div class="small-12 medium-3">
                    <strong><a href="plan-intermedio.php">INTERMEDIO</a></strong>
                  </div>
                  <div class="small-12 medium-3">
                    <strong><a href="plan-avanzado.php">AVANZADO</a></strong>
                  </div>
                  <div class="small-12 medium-3">
                    <strong><a href="plan-autoadministrable.php">AUTOADMINISTRABLE</a></strong>
                  </div>
                </div>
                <br><br>
                <div style="font-size:0.7em" class="row links">
                  <div class="small-12 medium-2">
                    Diseño Web
                  </div>
                  <div class="small-12 medium-2">
                    A la medida
                  </div>
                  <div class="small-12 medium-2">
                    WordPress
                  </div>
                  <div class="small-12 medium-2">
                    Responsive
                  </div>
                  <div class="small-12 medium-2">
                    Móbiles
                  </div>
                  <div class="small-12 medium-2">
                    Pymes
                  </div>
                </div>
              </div>
            </div>
          </footer>
          <!--<a href="index.html" class="button">sdfsdf</a>-->
          <script src="js/vendor/jquery.js"></script>
          <script src="js/vendor/what-input.js"></script>
          <script src="js/vendor/foundation.js"></script>
          <script src="js/app.js"></script>
        </div>
      </div>
    </div>
  </body>
</html>