
<ul class="vertical menu menu-lateral" data-accordion-menu>
  <li><a href="index.php">INICIO</a></li>
  <li>
    <a href="#">PLANES</a>
    <ul class="menu vertical nested">
      <li><a href="plan-basico.php">BASICO</a></li>
      <li><a href="plan-intermedio.php">INTERMEDIO</a></li>
      <li><a href="plan-avanzado.php">AVANZADO</a></li>
      <li><a href="plan-autoadministrable.php">AUTOADMINISTRABLE</a></li>
    </ul>
  </li>
  <li><a href="politicas.php">POLITICAS</a></li>
  <li>
    <a href="#">SOPORTE</a> 
    <ul class="menu vertical nested">
      <li><a href="soporte-correos-electronicos.php">CORREOS ELECTRONICOS</a></li>
    </ul>
  </li>
  <li><a href="contacto.php">CONTACTO</a></li>
</ul>