<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soporte | Correos Electrónicos</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/pricing_table.css">
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
          <button class="close-button" aria-label="Close menu" type="button" data-close>
          <span aria-hidden="true">&times;</span>
          </button>
          <!-- Menu Lateral -->
          <?php require 'codigos/lateral-menu.php';?>
        </div>
        <div class="off-canvas-content" data-off-canvas-content>
          <?php require 'codigos/top-menu.php';?>
          <h2 style="font-size: 2em">Correos Electrónicos</h2>
          <div class="claro">
            <div class="row align-middle" style="padding-bottom: 2em; padding-top: 2em">
              <div class="small-12 medium-12 large-12 columns">
                <div class="recuadro-detalles" style="padding-left: 1em; padding-right: 1em">
                  <div class="row">
                    <div class="small-12 columns">
                      <h4>Creación</h4>
                      <p>
                        Al momento de realizar el primer abono, debe enviar el nombre de los correos deseados, se le enviarán de vuelta los datos con claves genéricas, las cuales usted mismo debe cambiar accediendo al "WebMail". Solo debe anteponer "webmail." a su dominio como muestra la siguiente imágen (nota: para leer sus correos se recomienda <strong>Roundcube</strong>)
                      </p>
                      <img class="float-center sombra-imagen" src="imagenes/webmail.png">
                      <br>
                      <img class="float-center sombra-imagen" src="imagenes/cambio-contrasena.png">
                      <br>
                      <h4>Correo en dispositivos</h4>
                      <p>
                        Utiliza un gestor de correos, muchos dispositivos lo traen por defecto, puedes buscar alternativas como <strong>Microsoft Outlook, Gmail, Inbox, CloudMagic Email</strong>, etc. Una vez abierta la aplicacion ingresa tus datos y selecciona configuración manual:
                      </p>
                      <img class="float-center sombra-imagen" style="max-width: 300px" src="imagenes/correo-tutorial1.png">
                      <h4>Seleccionamos un protocolo</h4>
                      <p>
                        El protocolo <strong>IMAP</strong>, de forma predeterminada, permite al usuario conservar todos los mensajes en el servidor. Constantemente se sincroniza el programa de correo electrónico con el servidor, mostrando los mensajes que están presentes en la carpeta en cuestión. Todas las acciones realizadas en los mensajes (leer, mover, eliminar…) se realizan directamente en el servidor.
                      </p>
                      <p>
                        El protocolo <strong>POP</strong>, por defecto, está configurado para descargar todos los mensajes del servidor de correo electrónico al dispositivo desde el que se conecta. Esto significa que todas las acciones realizadas en los mensajes (leer, mover, borrar…) se realizarán en el propio dispositivo. Al descargarse, por defecto, se eliminan los mensajes del servidor y, por ello, el usuario no podrá volver a ver los mensajes desde cualquier lugar que no sea el dispositivo en el que los mensajes han sido descargados.
                      </p>
                      <img class="float-center sombra-imagen" style="max-width: 300px" src="imagenes/correo-tutorial2.png">
                      <h4>Datos servidor entrante</h4>
                      <p>
                        Servidor entrante: <strong>element.websitewelcome.com</strong><br>
                        IMAP Puerto: <strong>993</strong><br>
                        POP3 Puerto: <strong>995</strong><br>
                        Se recomienda <strong>SSL</strong> como protocolo de seguridad
                      </p>
                      <img class="float-center sombra-imagen" style="max-width: 300px" src="imagenes/correo-tutorial3.png">
                      <h4>Datos servidor saliente</h4>
                      <p>
                        Servidor saliente: <strong>element.websitewelcome.com</strong><br>
                        SMTP Puerto: <strong>465</strong><br>
                        Se recomienda <strong>SSL</strong> como protocolo de seguridad
                      </p>
                      <img class="float-center sombra-imagen" style="max-width: 300px" src="imagenes/correo-tutorial4.png">
                      <h4>Últimos datos</h4>
                      <p>Las configuraciones finales son preferenciales, generalmente es ideal dejar todo por defecto</p>
                      <img class="float-center sombra-imagen" style="max-width: 300px" src="imagenes/correo-tutorial5.png">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer>
            <span class="titulo">Te gusta la idea?</span><br><br>
            <a href="contacto.php"><button class="button" style="font-size: 1.2em">Contáctate</button></a><br><br>
            Correo electrónico: <span class="verde">contacto@difusionweb.cl</span> | teléfono: (+56) 9-62342699 
          </footer>
          <!--<a href="index.html" class="button">sdfsdf</a>-->
          <script src="js/vendor/jquery.js"></script>
          <script src="js/vendor/what-input.js"></script>
          <script src="js/vendor/foundation.js"></script>
          <script src="js/app.js"></script>
        </div>
      </div>
    </div>
  </body>
</html>