<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plan Avanzado</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/pricing_table.css">
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
          <button class="close-button" aria-label="Close menu" type="button" data-close>
          <span aria-hidden="true">&times;</span>
          </button>
          <!-- Menu Lateral -->
          <?php require 'codigos/lateral-menu.php';?>
        </div>
        <div class="off-canvas-content" data-off-canvas-content>
          <?php require 'codigos/top-menu.php';?>
          <h2 style="font-size: 2em">Plan Avanzado $xx.xxx</h2>
          
            <div class="row align-middle atributos">
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles1.png" alt="Dominio Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>Dominio del sitio</h4>
                      Registro del dominio tipo www.miempresa.cl incluído, deberá cancelar $10.000 a partir del segundo año y de forma anual.
                    </div>
                  </div>
                </div>
              </div>
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles2.png" alt="Hosting Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>Alojamiento (Hosting)</h4>
                      Alojamiento de sus archivos por un año (2gb), deberá cancelar $10.000 a partir del segundo año y de forma anual.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          
            <div class="row align-middle atributos">
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles3.png" alt="Páginas Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>10 Páginas</h4>
                      Usted elige las páginas que componen su sitio web con un tope de 10. Dentro de las más habituales están: Quiénes Somos, Productos, Servicios, Equipo, etc….
                    </div>
                  </div>
                </div>
              </div>
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles4.png" alt="Hosting Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>10 Correos</h4>
                      Con un máximo de 200mb por cada uno, puede tener hasta 2 correos tipo "nombre@miempresa.cl".
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          
            <div class="row align-middle atributos">
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles12.png" alt="Dominio Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>Diseño Adaptable</h4>
                      Todos los elementos gráficos cámbian de tamaño según el dispositivo desde el cual se navega, entregando una excelente experiancia de uso en tables, celulares, netbooks, etc. 
                    </div>
                  </div>
                </div>
              </div>
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles6.png" alt="Hosting Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>Motores de búsqueda</h4>
                      Registro en Google, Yahoo y Bing para acelerar la indexacion de tu sitio web.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          
            <div class="row align-middle atributos">
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles7.png" alt="Dominio Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>20 Imágenes</h4>
                      Un máximo de 20 imágenes para la galería de fotos, incluyendo edición y búsqueda. Si necesitas más puedes llegar a un acuerdo por correo electrónico. 
                    </div>
                  </div>
                </div>
              </div>
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles8.png" alt="Hosting Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>Sección de contacto</h4>
                      Formulario con preguntas variadas, datos de contacto y ubicación de la empresa usando Google Maps.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          
            <div class="row align-middle atributos">
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles9.png" alt="Dominio Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>4 Actualizaciones</h4>
                      Anualmente tienes derecho a 2 actualizaciones, ya sea datos de contacto, cifras, imágenes, textos, etc. siempre y cuando se utilicen los mismos elementos gráficos del sitio.
                    </div>
                  </div>
                </div>
              </div>
              <div class="small-12 medium-6 large-6 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles10.png" alt="Hosting Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>Redes sociales</h4>
                      Iconos con links hacia tus redes sociales, manteniendo la estética del sitio, en el menú principal y/o pié de página.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          
            <div class="row align-middle atributos">
              <div class="small-12 medium-12 large-12 columns">
                <div class="recuadro-detalles">
                  <div class="row align-middle">
                    <div class="small-4 columns">
                      <img class="float-center" src="imagenes/detalles11.png" alt="Dominio Web">
                    </div>
                    <div class="small-8 columns">
                      <h4>15 Días hábiles</h4>
                      El cliente tiene un mayor impácto en el tiempo de desarrollo, debido a su propia agenda y responsabilidad para enviar datos necesarios de forma oportuna.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
          <footer>
            <span class="titulo">Te gusta la idea?</span><br><br>
            <a href="contacto.php"><button class="button" style="font-size: 1.2em">Contáctate</button></a><br><br>
            Correo electrónico: <span class="verde">contacto@difusionweb.cl</span> | teléfono: (+56) 9-62342699 
          </footer>
          <!--<a href="index.html" class="button">sdfsdf</a>-->
          <script src="js/vendor/jquery.js"></script>
          <script src="js/vendor/what-input.js"></script>
          <script src="js/vendor/foundation.js"></script>
          <script src="js/app.js"></script>
        </div>
      </div>
    </div>
  </body>
</html>