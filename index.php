<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Difusion Web</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/pricing_table.css">
    
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


   <style>
        
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('img/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('img/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
      </style>

</head>
<body>
<div class="off-canvas-wrapper">
<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
      
      <!-- Menu Lateral -->
      <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
        <button class="close-button" aria-label="Close menu" type="button" data-close>
          <span aria-hidden="true">&times;</span>
        </button>      
      <?php require 'codigos/lateral-menu.php';?>
      </div>

      <div class="off-canvas-content" data-off-canvas-content>

        <?php require 'codigos/top-menu.php';?>
        <?php require 'codigos/slider-index.php';?>

        <div class="fondo-planes">
          
          <section id="pricePlans">
            
            <h1>PLANES DISPONIBLES</h1>
            
            <ul id="plans">
              <li class="plan">
                <ul class="planContainer">
                  <li class="title"><h2>Básico</h2></li>
                  <li class="price"><p>$xx.xxx</p></li>
                  <li>
                    <ul class="options">
                      <li>2 <span>correos electrónicos</span></li>
                      <li>.cl <span>Dominio incluído</span></li>
                      <li>1 año <span>Hosting incluído</span></li>
                      <li>5 <span>páginas</span></li>
                      <li>2 <span>actualizaciones anuales</span></li>
                      <li>Diseño <span>estándar</span></li>
                    </ul>
                  </li>
                  <li class="buttonn"><a href="plan-basico.php">Ver Más</a></li>
                </ul>
              </li>

              <li class="plan">
                <ul class="planContainer">
                  <li class="title"><h2 class="bestPlanTitle">Intermedio</h2></li>
                  <li class="price"><p class="bestPlanPrice">$xx.xxx</p></li>
                  <li>
                    <ul class="options">
                      <li>5 <span>correos electrónicos</span></li>
                      <li>.cl <span>Dominio incluído</span></li>
                      <li>1 año <span>Hosting incluído</span></li>
                      <li>10 <span>páginas</span></li>
                      <li>4 <span>actualizaciones anuales</span></li>
                      <li>Diseño <span>estándar</span></li>
                    </ul>
                  </li>
                  <li class="buttonn"><a class="bestPlanButton" href="plan-intermedio.php">Ver Más</a></li>
                </ul>
              </li>

              <li class="plan">
                <ul class="planContainer">
                  <li class="title"><h2>Avanzado</h2></li>
                  <li class="price"><p>$xx.xxx</p></li>
                  <li>
                    <ul class="options">
                      <li>10 <span>correos electrónicos</span></li>
                      <li>.cl <span>Dominio incluído</span></li>
                      <li>1 año <span>Hosting incluído</span></li>
                      <li>10 <span>páginas</span></li>
                      <li>4 <span>actualizaciones anuales</span></li>
                      <li>Diseño <span>adaptable</span></li>
                    </ul>
                  </li>
                  <li class="buttonn"><a href="plan-avanzado.php">Ver Más</a></li>
                </ul>
              </li>

              <li class="plan">
                <ul class="planContainer">
                  <li class="title"><h2>Autoadministrable</h2></li>
                  <li class="price"><p>$xx.xxx</p></li>
                  <li>
                    <ul class="options">
                      <li>10 <span>correos electrónicos</span></li>
                      <li>.cl <span>Dominio incluído</span></li>
                      <li>1 año <span>Hosting incluído</span></li>
                      <li>10 <span>páginas</span></li>
                      <li>4 <span>actualizaciones anuales</span></li>
                      <li>Diseño <span>adaptable</span></li>
                    </ul>
                  </li>
                  <li class="buttonn"><a href="plan-autoadministrable.php">Ver Más</a></li>
                </ul>
              </li>
            </ul>
    
          </section>
        </div>

        <h2 class="titulo-beneficios">Beneficios Destacados</h2>
        
        <div class="claro">
          <div class="row align-middle" style="padding-bottom: 4em; padding-top: 2em">
            <div class="small-12 medium-6 large-6 columns"><img src="imagenes/features-min.jpg"></div>
            <div class="small-12 medium-6 large-6 columns" style="text-align: justify; font-family:'Open Sans'">
                <h4>Correos Empresariales:</h4><br>
                <ul>
                <li><strong>IMPRESIÓN</strong>: Un correo tipo <span>contacto@techcorp.cl</span> le da a tus clientes una imagen mas profesional que contact0techcorp@gmail.com</li><br>
                <li><strong>PUBLICIDAD</strong>: Cada vez que envías un correo estás publicitando tu propia empresa, y no hotmail, gmail, etc.</li><br>
                <li><strong>ORDEN</strong>: Tener un correo por cada departamento/cargo ayuda a mejorar el <span>flujo de la información.</span></li><br>
                <li><strong>ALCANCE</strong>: Gracias a tu correo empresarial tus potenciales clientes pueden buscar fácilmente más información sobre tu empresa.</li>
                </ul>
            </div>
          </div>
        </div>

        <div class="obscuro">
          <div class="row align-middle">
            <div class="small-12 medium-6 large-6 columns" style="text-align: justify">
              <h4>Diseños Adaptables:</h4>
              <br>
              <ul>
              <li><strong>MÁS VISITAS</strong>: No pierdas esos posibles clientes que visitan tu sitio web desde un dispositivo móvil, entrégales toda la informacion de la mejor forma posible.</li><br>
              <li><strong>MULTI-DISPOSITIVOS</strong>: No importa si es un IPAD, un SmartPhone o una pantalla de alta resolución, el contenido de tu sitio web siempre se verá perfectamente.</li><br>
              <li><strong>MEJOR USABILIDAD</strong>: La navegación por el sitio debe ser de forma intuitiva, aprovechando al máximo los recursos disponibles, sobretodo las funciones táctiles.</li><br>
              <li><strong>COSTO-EFICIENTE</strong>: Disminuye el costo de mantener un sitio web para móviles y uno para computadores de escritorio, junto con la dedicacion que cada uno necesita.</li>
              </ul>
            </div>
            <div class="small-12 medium-6 large-6 columns"><img src="imagenes/features2-min.png"></div>
          </div>
        </div>

        <div class="claro">
          <div class="row align-middle" style="padding-bottom: 4em; padding-top: 2em">
            <div class="small-12 medium-6 large-6 columns"><img src="imagenes/features3-min.jpg"></div>
            <div class="small-12 medium-6 large-6 columns" style="text-align: justify; font-family:'Open Sans'">
              <h4>Registro en Buscadores:</h4>
              <br>
              <ul>
              <li><strong>CONOCIMIENTO DE MARCA</strong>: Inscribir tu sitio web en los búscadores más populares hace más rápido la indexación de este, <span>un nombre único</span> fácilmente aparecerá en los primeros resultados.</li>
              <br>
              <li><strong>PALABRAS CLAVES</strong>: El uso de etiquetas en los elementos de tu sitio facilita la búsqueda para tus posibles clientes.</li><br>
              <li><strong>ESTADÍSTICAS</strong>: Con la herramienta <span>Google Analytics</span> puedes llevar un seguimiento de las visitas hacia tu sitio web, los datos se actualizan cada 24 horas.</li><br>
              </ul>
            </div>
          </div>
        </div>

        <div class="obscuro">
          <div class="row align-middle">
            <div class="small-12 medium-6 large-6 columns" style="text-align: justify">
                <h4>Actualizaciones Anuales:</h4>
                <br>
                <ul>
                <li><strong>DATOS DE CONTACTO</strong>: Ya sea direcciones de correo, direcciones físicas, nombres, teléfonos de contacto, etc. Puedes pedir una actualizacion periódicamente.</li><br>
                <li><strong>IMÁGENES</strong>: Actualiza fotografías de productos, equipamientos, trabajos, etc. Mantén al día toda información gráfica hacia tus clientes.</li><br>
                <li><strong>TEXTOS</strong>: Tu empresa está progresando? no olvides renovar tus prodecimientos de trabajo, nuevos servicios, testimonios, cualquier cámbio siempre y cuando se usen los mismos elementos gráficos del sitio.</li><br>
                </ul>
            </div>
            <div class="small-12 medium-6 large-6 columns"><img src="imagenes/features4-min.png"></div>
          </div>
        </div>

        <div class="claro">
          
          <h4 style="text-align: center; padding-top:1em">METODOLOGÍA DE TRABAJO</h4><br>

          <div class="row align-middle">

            <div class="small-12 medium-6 large-3 columns"><img  style="margin-right: auto; margin-left: auto; display: block" src="imagenes/procedimientos-min.jpg"></div>
            <div class="small-12 medium-6 large-3 columns"><img style="margin-right: auto; margin-left: auto; display: block" src="imagenes/procedimientos2-min.jpg"></div>
            <div class="small-12 medium-6 large-3 columns"><img style="margin-right: auto; margin-left: auto; display: block" src="imagenes/procedimientos3-min.jpg"></div>
            <div class="small-12 medium-6 large-3 columns"><img style="margin-right: auto; margin-left: auto; display: block" src="imagenes/procedimientos4-min.jpg"></div>
            
          </div>
        </div>

        <footer>
        <span class="titulo">Te gusta la idea?</span><br><br>
        <a href="contacto.php"><button class="button" style="font-size: 1.2em">Contáctate</button></a><br><br>
        Correo electrónico: <span class="verde">contacto@difusionweb.cl</span> | teléfono: (+56) 9-62342699 
        </footer>

    

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.js"></script>
<script src="js/app.js"></script>

      </div>
</div>
</div>
</body>
</html>
