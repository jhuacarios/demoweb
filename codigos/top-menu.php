
<div class="contenedor-menu-top">
<div class="top-bar" style="max-width: 1245px; margin-left: auto; margin-right: auto">
  <div class="top-bar-left">
    <button type="button" class="menu-icon show-for-small-only" data-toggle="offCanvas"></button>
    <div class="minilogo">
      <a href="index.php"><img src="imagenes/minilogo.png" style="height: 40px" /></a>
    </div>
  </div>
  <div class="top-bar-right">
    <ul class="dropdown menu align-right" data-dropdown-menu>
      <li><a href="index.php">INICIO</a></li>
      <li>
        <a href="#">PLANES</a>
        <ul class="menu vertical">
          <li><a href="plan-basico.php">BASICO</a></li>
          <li><a href="plan-intermedio.php">INTERMEDIO</a></li>
          <li><a href="plan-avanzado.php">AVANZADO</a></li>
          <li><a href="plan-autoadministrable.php">AUTOADMINISTRABLE</a></li>
        </ul>
      </li>
      <li><a href="politicas.php">POLITICAS</a></li>
      <li>
        <a href="#">SOPORTE</a>
        <ul class="menu vertical">
          <li><a href="soporte-correos-electronicos.php">CORREOS ELECTRONICOS</a></li>
        </ul>
      </li>
      <li><a href="contacto.php">CONTACTO</a></li>
    </ul>
  </div>
</div>
</div>