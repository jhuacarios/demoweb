<!doctype html>
<html class="no-js" lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Políticas</title>
    <link rel="stylesheet" href="css/foundation.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/pricing_table.css">
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/jssor.slider.mini.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-left" id="offCanvas" data-off-canvas>
          <button class="close-button" aria-label="Close menu" type="button" data-close>
          <span aria-hidden="true">&times;</span>
          </button>
          <!-- Menu Lateral -->
          <?php require 'codigos/lateral-menu.php';?>
        </div>
        <div class="off-canvas-content" data-off-canvas-content>
          <?php require 'codigos/top-menu.php';?>
          <h2 style="font-size: 2em">Políticas</h2>
          <div class="claro">
            <div class="row align-middle" style="padding-bottom: 2em; padding-top: 2em">
              <div class="small-12 medium-12 large-12 columns">
                <div class="recuadro-detalles" style="padding-left: 1em; padding-right: 1em">
                  <div class="row">
                    <div class="small-12 columns">
                      <h4>Privacidad</h4>
                      Nos comprometemos a mantener en privado sus datos personales de terceras personas. Con respecto a sus correos, en ningún momento violaremos su privacidad, a menos que necesite algún tipo de soporte técnico, sin embargo, el cliente debe comprometerse a hacer buen uso de su correo electrónico, o podría ser entregado a las autoridades correspondientes si es solicitado por estas.
                      <br>
                      <br>
                      <h4>Exclusión del servicio</h4>
                      Está estrictamente prohibido el envío de SPAM (Correo Masivo no Deseado). Cualquier cliente sorprendido efectuando dicha práctica será eliminado y/o suspendido colocando un mensaje de suspensión en el sitio web del Cliente sin previo aviso y sin derecho a reintegro de dinero cancelado.
                      <br>
                      <br>
                      <h4>Tarifas</h4>
                      Las tarifas pueden cambiar durante el tiempo, sin embargo, una vez realizada la cotizacion, se mantendrá la tarifa de aquel instante, junto con los pagos anuales a futuro.
                      <br>
                      <br>
                      <h4>Plazos</h4>
                      Los tiempos de entrega son aproximados, el cliente cumple un papel importante entregando la información requerida con el menor retraso posible.
                      <br>
                      <br>
                      <h4>Contenido</h4>
                      <p>
                        Difusionweb.cl se reserva el derecho de no publicar contenido que traspase las normas morales y éticas. 
                      </p>
                      <p>
                        Difusionweb.cl no se hace responsable del contenido publicado (artículos, comentarios, ofertas y/o promociones, etc.) por los clientes en sus sitios web, siendo únicamente éstos los únicos responsables.
                      </p>
                      <p>
                        Usted autoriza a Difusionweb.cl a incluir un enlace de de texto y/o imagen de vuelta a nuestro sitio web (BackLink), y a ubicar su sitio en nuestro portafolio.
                      </p>
                      <p>
                        El cliente podrá solicitar los respaldos del sitio web solamente si el servicio se encuentra activo, bajo ninguna circunstancia se entregará respaldos con el servicio suspendido o dado de baja.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer>
            <span class="titulo">Te gusta la idea?</span><br><br>
            <a href="contacto.php"><button class="button" style="font-size: 1.2em">Contáctate</button></a><br><br>
            Correo electrónico: <span class="verde">contacto@difusionweb.cl</span> | teléfono: (+56) 9-62342699 
          </footer>
          <!--<a href="index.html" class="button">sdfsdf</a>-->
          <script src="js/vendor/jquery.js"></script>
          <script src="js/vendor/what-input.js"></script>
          <script src="js/vendor/foundation.js"></script>
          <script src="js/app.js"></script>
        </div>
      </div>
    </div>
  </body>
</html>